<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_inlineCheckbox1</name>
   <tag></tag>
   <elementGuidId>3949ab60-7dfa-40a6-9b0f-d7b6878a7e59</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inlineCheckbox1']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#inlineCheckbox1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3715f35e-6011-439a-b3ae-85897fcbc878</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>003ebbc4-a677-4a3e-8cc7-114d8ced6a48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>de50e611-0f3e-4d46-b9ab-6596c8f4a7fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inlineCheckbox1</value>
      <webElementGuid>576197be-22b3-4fbd-bf12-6843e39626ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>option1</value>
      <webElementGuid>4bcee736-4245-4e4d-97eb-b1bf38de631c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inlineCheckbox1&quot;)</value>
      <webElementGuid>18ff72b1-6bac-4864-a8bd-723bc2be4855</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inlineCheckbox1']</value>
      <webElementGuid>6083362e-4bab-4250-bf90-535722c85f48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/input</value>
      <webElementGuid>fbb62d68-3578-44ed-a65a-6ba13af0b3f6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'inlineCheckbox1']</value>
      <webElementGuid>90e38d7f-65f6-402f-b10e-de4173e6a568</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
