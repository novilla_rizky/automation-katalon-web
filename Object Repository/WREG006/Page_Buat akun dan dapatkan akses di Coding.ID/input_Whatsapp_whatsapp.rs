<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Whatsapp_whatsapp</name>
   <tag></tag>
   <elementGuidId>d61528fb-bbfe-431f-a15e-e10660b2da9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='whatsapp']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#whatsapp</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3e4d7afc-2ae0-40e3-b4a1-5521bf076ea4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>07a52d2b-4ca3-439d-b772-c2b82d82c1bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>number</value>
      <webElementGuid>fde741c1-9071-4d28-87c6-1478897be410</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>WhatsApp </value>
      <webElementGuid>4a772e9d-9705-46c4-958f-50cd5204613b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control </value>
      <webElementGuid>569a0a57-df2e-4e77-b5f5-26c3edca670d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>79bd4ef2-e481-4ba6-bbaa-36e08ef02f3a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>whatsapp</value>
      <webElementGuid>ca955f25-a009-45f0-b5c5-c8356d08034a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;whatsapp&quot;)</value>
      <webElementGuid>d2258aee-21c2-4415-89ad-779df61e4ecc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='whatsapp']</value>
      <webElementGuid>225a9179-baaa-4390-a896-8d7545545aeb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/input</value>
      <webElementGuid>79c22a38-ef44-442f-80a9-86f6accd0c75</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'whatsapp' and @type = 'number' and @placeholder = 'WhatsApp ' and @name = 'whatsapp']</value>
      <webElementGuid>004e4df8-c4e5-4c9f-85b9-674b62f807dc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
