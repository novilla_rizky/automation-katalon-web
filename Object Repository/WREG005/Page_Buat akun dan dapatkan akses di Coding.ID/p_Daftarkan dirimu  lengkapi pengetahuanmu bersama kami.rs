<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Daftarkan dirimu  lengkapi pengetahuanmu bersama kami</name>
   <tag></tag>
   <elementGuidId>ba91140d-3b55-4f7b-a4f9-ef39dcacdfc9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::p[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.wm-fancy-title > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>6715df34-0cb5-4453-a35d-cf74cd7ee6f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami</value>
      <webElementGuid>fd7314dd-4c3b-45b3-919b-2f5b5ad5ea99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;wm-fancy-title&quot;]/p[1]</value>
      <webElementGuid>816edda0-37cf-4f2a-bfc0-64d39e43aba8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat Akun Baru'])[1]/following::p[1]</value>
      <webElementGuid>baab5ffc-e854-4575-9583-6eb2aa6460c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nama'])[1]/preceding::p[1]</value>
      <webElementGuid>4cc72757-93d3-4cd0-8370-d4cbb515b04e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal lahir'])[1]/preceding::p[1]</value>
      <webElementGuid>a9953a81-fac9-4d6d-94bc-5058c9a61c41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami']/parent::*</value>
      <webElementGuid>71d89109-a77f-4175-87fe-fb3e4f1a1bc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/p</value>
      <webElementGuid>a2123e63-809a-4e3b-bdef-c9d64ab5835a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = ' Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami' or . = ' Daftarkan dirimu &amp; lengkapi pengetahuanmu bersama kami')]</value>
      <webElementGuid>7e5a3b8f-60c1-4c33-be0c-3d3c843c67b1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
