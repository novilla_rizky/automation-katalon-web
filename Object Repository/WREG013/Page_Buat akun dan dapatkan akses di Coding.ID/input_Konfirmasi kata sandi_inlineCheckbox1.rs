<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Konfirmasi kata sandi_inlineCheckbox1</name>
   <tag></tag>
   <elementGuidId>df50af7d-8a8f-4e39-b4c8-1666e85afaa4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#inlineCheckbox1</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='inlineCheckbox1']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>579efffc-7a77-4537-8e24-c1fa78b4a79c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-check-input</value>
      <webElementGuid>cee6b84a-c78c-45fb-9988-7ede2cfd98b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>83f95370-286b-4aa1-be03-197aea33be88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>inlineCheckbox1</value>
      <webElementGuid>53025098-a28b-4161-beba-e492ba028e5d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>option1</value>
      <webElementGuid>53be26d4-330f-4310-b99a-e6a73ef79ee9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;inlineCheckbox1&quot;)</value>
      <webElementGuid>7da576bd-f3fa-4c53-b320-21492e66fe97</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='inlineCheckbox1']</value>
      <webElementGuid>d9248700-61ce-4953-94ea-50a739bf4087</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/input</value>
      <webElementGuid>8bfcf352-899c-4ada-be5d-be628cc0e05f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'inlineCheckbox1']</value>
      <webElementGuid>10b4eb2a-f916-4b35-b80b-d0df8121193a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
