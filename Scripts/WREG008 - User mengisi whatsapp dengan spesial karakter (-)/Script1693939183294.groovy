import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/')

WebUI.click(findTestObject('Object Repository/WREG008/Page_Be a Profressional Talent with Coding.ID/button_Buat                                _86b528'))

WebUI.setText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), 
    'sumanti')

WebUI.setText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    '1-dec-2007')

WebUI.sendKeys(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), 
    'sumanti@yahoo.com')

WebUI.setText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), 
    '-')

WebUI.setEncryptedText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'), 
    'fW9ZQjjT/iS/C1EApsj8Aw==')

WebUI.setEncryptedText(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'), 
    'fW9ZQjjT/iS/C1EApsj8Aw==')

WebUI.click(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyEqual(WebUI.getAttribute(findTestObject('Object Repository/WREG008/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), 
        'validationMessage'), 'Please enter a number.', FailureHandling.CONTINUE_ON_FAILURE)

WebUI.takeScreenshot()

WebUI.closeBrowser()

